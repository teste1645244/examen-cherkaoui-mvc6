-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 21 fév. 2024 à 09:20
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `exam`
--

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sujet` varchar(150) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_general_ci NOT NULL,
  `message` text COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `sujet`, `email`, `message`, `created_at`) VALUES
(1, 'weipfhweifbwepifewoeifewef', 'koko@gmail.com', 'oiwbefoiwebfoirgbwoibwroigwegirboiwbgoiwrbowbwoibwoibwroibwoibwobwrboibrobiwbboib', '2024-02-21 09:39:14'),
(2, 'weipfhweifbwepifewoeifewef', 'cherka8.jebril@gmail.com', 'lkaefpqeifhqpi;vaipvnfqiopbgipbgeiwqbveqoifbqofibqwoifbqwoifbqoifbqwofibqwoifb', '2024-02-21 09:40:03'),
(3, 'weipfhweifbwepifewoeifewef', 'admin@gmail.com', 'kpikhriphqpfhqpifhipqhfpiqwfhfiwipfihqwpifhqwpifhpiqwhf', '2024-02-21 09:40:46'),
(4, 'weipfhweifbwepifewoeifewef', 'cherka8.jebril@gmail.com', 'lkengfkpwenbvlkqfboeqlfbqowifbqwoifbwoifbqwoifbqwoifbqwofibwqfobi', '2024-02-21 09:41:16'),
(5, 'weipfhweifbwepifewoeifewef', 'admin@gmail.com', 'ioehfioqhfoqiwfhqwoifhqwoifhqwfoiqwhfoiqwhfoqwifh', '2024-02-21 09:42:29'),
(6, 'weipfhweifbwepifewoeifewef', 'admin@gmail.com', 'wefljebflkqbfqwklfbqlfbqwlfbqwlfbwqljfbqwjlkqwbjlfb', '2024-02-21 09:43:48'),
(7, 'weipfhweifbwepifewoeifewef', 'admin@gmail.com', 'ofhefhfoefhoqhqfhqfhiqfwohiqfhioqfwhoifhoiqqwfoi', '2024-02-21 09:45:40'),
(8, 'weipfhweifbwepifewoeifewef', 'admin@gmail.com', 'qwfqwfhoqoqhoqfhqwfhiwoqqqwfhiofqwhoiqfwiho', '2024-02-21 09:46:52'),
(9, 'weipfhweifbwepifewoeifewef', 'kalibrouille@gmail.com', ';kmwegm;kek;feqk;fnqe;fkq;fohjqwpfojwqpofjqwpofqwfpoj', '2024-02-21 09:47:55'),
(10, 'weipfhweifbwepifewoeifewef', 'admin@gmail.com', 'k;avjkwfkfwiwfqhioqfhofqwhoiqfwoiqwfoihqfwoihqfwi', '2024-02-21 09:48:12'),
(11, 'weipfhweifbwepifewoeifewef', 'admin@gmail.com', 'lkqhfoiqwhfiopqfhoiqwhfoiqwfhqowifhqwoifhqwoifhqw', '2024-02-21 09:49:55'),
(12, 'weipfhweifbwepifewoeifewef', 'admin@gmail.com', ';kjfewpifjqpefjpofjqpofjqfwpofj', '2024-02-21 10:10:33');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
